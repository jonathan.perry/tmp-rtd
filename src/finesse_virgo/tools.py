import finesse

# URLS where data files are stored
DATAFILES = {
    "LIGO_axialsymmetric_test_mass_reciprocity.npz": "https://zenodo.org/record/6385930/files/LIGO_axialsymmetric_test_mass_reciprocity.npz",
}

CHECKSUM = {
    "LIGO_axialsymmetric_test_mass_reciprocity.npz": "a9ee7fd79609b58cde587e345ee78fd6"
}


def download(datafile):
    if datafile not in DATAFILES:
        raise FileNotFoundError(f"Datafile {datafile} is not an option")
    if datafile not in CHECKSUM:
        raise RuntimeError(f"Datafile {datafile} does not have a checksum specified")

    from tqdm.auto import tqdm
    import requests
    from pathlib import Path
    import shutil
    import hashlib

    # Get data installation path from finesse config
    cfg = finesse.config.config_instance()
    path = Path(cfg["finesse.data"]["path"]).expanduser().absolute() / "finesse-virgo"
    path.mkdir(parents=True, exist_ok=True)
    print(f"Writing data to {path}")
    
    # make an HTTP request within a context manager
    with requests.get(DATAFILES[datafile], stream=True) as r:
        # check header to get content length, in bytes
        total_length = int(r.headers.get("Content-Length"))
        
        # implement progress bar via tqdm
        with tqdm.wrapattr(
            r.raw, "read", total=total_length, desc=f"Downloading {DATAFILES[datafile]}"
        ) as raw:
            # save the output to a file
            with open(path / datafile, "wb") as output:
                shutil.copyfileobj(raw, output)
    
    # compare checksum
    with open(path / datafile, "rb") as output:
        checksum = hashlib.md5(output.read()).hexdigest()
        if checksum != CHECKSUM[datafile]:
            raise RuntimeError(
                f"Checksum failed, downloaded file probably corrupted: {checksum} != {CHECKSUM[datafile]}"
            )
