

Introduction
============

| ``Finesse`` is a simulation program for interferometers. The user can
  build any kind of virtual interferometer.
| This is a handout for AdVirgo+ ``KAT`` file, Finesse input text file
  describing the interferometer in the form of components and connecting
  nodes. The purpose of this document is to review and motivate the
  parameters in the file, as well as to keep track of changes in the
  file.

IFO Matching
------------

As from TDR
(`VIR–0128A–12 <https://tds.virgo-gw.eu/?content=3&r=9317>`__), the
required values for PR and SR radius of curvature (RoC) is
:math:`1430.0` m, both the PRC and the SRC are marginally stable for
that value. The actual measured values for PRM and SRM are
:math:`1477.1` m
(`VIR-0029A-15 <https://tds.virgo-gw.eu/?content=3&r=11410>`__) and
:math:`1443.35` m
(`VIR-0028A-15 <https://tds.ego-gw.it/?content=3&r=11409>`__),
respectively. In the ``KAT`` file the RoCs are set to the design value
(:math:`1430.0` m) and the compensation plates (CP) focal length is such
that the PRC and SRC are well matched to the arm cavities, even though
this requires a divergent compensation plate
(`VIR-0629A-18 <https://tds.virgo-gw.eu/?content=3&r=14682>`__).

Update
^^^^^^

December 7, 2021 - the focal length of the CPs have been further
optimised to mode match the recycling cavities to the arm cavities.

.. _omc:

The OMC modelling
-----------------

| The OMC simulated with Finesse for Ad.Virgo (:numref:`fig_omc`)
  used most of the measured values reported in `LB
  50572 <https://logbook.virgo-gw.eu/virgo/?r=50572>`__, and all the
  requirements reported in
  `VIR-0535A-21 <https://tds.virgo-gw.eu/?content=3&r=18807>`__. In
  :numref:`tab_tomc` are reported some of the OMC
  parameters measured in Cascina.
| The *Loss* and the *Throughout* are derived from some measures made at
  LAPP (ref.person: Michal Was).

.. _tab_tomc:  

.. table:: This table resume the OMC parameters measured in Cascina

   =============== ==================
   Finesse         :math:`1100`
   FSR             :math:`834` MHz
   :math:`T_{In}`  :math:`0.285`\ %
   :math:`T_{Out}` :math:`0.285`\ %
   :math:`T_{HR1}` :math:`1.1`\ ppm
   :math:`T_{HR2}` :math:`1.3`\ ppm
   Throughout      :math:`\approx2\%`
   Loss [1]_       :math:`60`\ ppm
   =============== ==================

| The Finesse value was inferred in the following way:
| starting from a theoretical FSR value of 834MHz, the Finesse value was
  raised until the tails of the Airy’s curve (red line in :numref:`fig_airy`)
  matched the data plot. This lead to a measured Finesse of 1100.

.. _fig_airy:
.. figure:: images/OMC_figure1.png
   :alt: Matching tails between the Airy’s curve and the real data plot.
   :name: fig:airy
   :width: 12cm

   Matching tails between the Airy’s curve and the real data plot.

| From this Finesse the transmissivities values of the Input and Output
  mirrors were inferred using the following approximation:

  .. math:: \mathcal{F}\approx \frac{\pi}{1-r_1r_2r_3r_4}

  Where all the :math:`r` are amplitude reflectivities. Here :math:`r_1`
  is taken equal to :math:`r_2`; them refers respectively to the input
  and output mirrors. Following :math:`r_3` refers to the HR curved
  mirror and :math:`r_4` refers to the flat HR mirror. Setting
  :math:`r_3\approx 1` (ideal HR mirror) and :math:`r_4=1-L-t_4`, were L
  is the cavity round trip loss and :math:`t_4` is the amplitude
  transmissivity of the second HR mirror. Computing the formula we find
  the transmissivities in power mentioned in
  :numref:`tab_tomc`. The 60ppm round trip losses
  measured in the cavity come from various types of scattering and
  absorption in the coating and in the substrate of the cavity; it
  roughly match the measured throughput.
| To insert this latter parameter in the simulation we place a fake
  :math:`T_4` coefficient on the BS indicated by :math:`OMC1\_4`, this
  trasmittivity contains both round trip loss and the two real T
  coefficients from the two HR mirrors, the visual scheme of the trick
  is showed in :numref:`fig_omc`. More detailed info can be found
  in [add notebook link].
| We running test on the OMC simulation file to see the match between
  the real data and the simulated one, the result is showed in
  :numref:`tab_sim`.

.. _fig_omc:  

.. figure:: images/omc_t_in.png
   :alt: Visual representation of the losses used in the simulation.
   :name: fig:omc
   :width: 14cm

   Visual representation of the losses used in the simulation.

.. _tab_sim:
   
.. table:: This table shows the data measured on the real OMC

   ========== ===================
   Finesse    :math:`1089.29`
   FSR        :math:`834.569` MHz
   Throughout 0.979
   Loss [2]_  :math:`0.006`
   ========== ===================

| 

We have chosen to keep the round trip loss number round in the
simulation, this lead to a little discrepancy between the measured
Finesse and the simulated one. However that doesn’t affect the overall
work of the simulated OMC, if it will be required in future the Finesse
values can be made equal.

.. _sec:Arms characterization:

Arms characterization
=====================

The arm lengths of the IFO are been measured with a FINESSE simulation,
the full code is in `Arms
characterization <https://git.ligo.org/virgo/isc/finesse/andreas_freise/-/blob/master/2021/arm_length/arm_length.ipynb>`__.

.. _fig_n_arm:

.. figure:: images/pow_b7.png
   :alt: Simulation result of the North arm’s measurement.
   :width: 12cm

   Simulation result of the North arm’s measurement.



| 
| The starting point was a discrepancy of the 56MHz sideband in the FSR
  scan, where the peacks were slightly shifted between the West and
  North arm results
  (`VIR-53000 <https://logbook.virgo-gw.eu/virgo/?r=53000>`__).
| Given the FSR of the carrier, in the FSR scan the :math:`56`\ MHz
  peacks have a frequency offset :math:`\Delta f` of:
| 

  .. math::

     \begin{aligned}
     North\,\,arm:\, & 0.4528\times FSR\\
     West\,\,arm:\, & 0.4416 \times FSR\,\,.
     \end{aligned}

  Knowing that:

  .. math:: f_{56MHz}=56436993MHz\,\,\, \text{and}\,\,\, FSR=\frac{c}{2L}

  We can then compute the arm lengths in few simple steps.
| For the North arm we have:

  .. math:: f = n\times FSR + \Delta f\\

  .. math:: 56436993 MHz= n\times FSR + 0.4528\times FSR\,\,.

  The closest round number :math:`n` that fit the equation is
  :math:`1129`, this lead to:

  .. math:: FSR=\frac{56436993MHz}{1129 + 0.4528}\,\,,

  so we have:

  .. math::

     MMT
       L_{North} = \frac{c}{2FSR}=2999.818m\,\,.

  The same procedure it’s been done for the West Arms. It leads to
  :math:`L_{West}=2999.788`,
  `VIR-1080A-21 <https://tds.virgo-gw.eu/?content=3&r=19516>`__.

.. _sec:EOM modulation lengths:

EOMs modulation depths 
=======================

The EOMs installed in Ad.Virgo+ are phase modulators. Each of them
requires an amplifier that gives power to the crystal that form the EOM;
in order to create the phase shift in the side bands. For the one that
produce the :math:`56` MHz sideband the amplifier of :math:`1` W was
replaced by one of :math:`2` W. This change lead to an electric signal
ampitude of :math:`0.12` dBm applied on the EOM, It can be monitorized
using INJ_LNFS_AMPL\ :math:`1`/:math:`2`/:math:`3` channels. The
corrisponding frequencies channels are
INJ_LNFS_FREQ\ :math:`1`/:math:`2`/:math:`3`. The change was made in
order to increase the modulation depth from :math:`0.16` rad to
:math:`0.25` rad, since the SNR value was too small to gain a good error
signal (|LB 38123 link|_).

.. |LB 38123 link| replace:: LB :math:`38123`
.. _LB 38123 link: https://logbook.virgo-gw.eu/virgo/?c=

.. _sec: mode-match:

Focal lengths optimisation for OMC’s mode-matching 
===================================================

The new parameters for the OMC in Ad.Virgo+ provided some mode mismatch
inside the Virgo kat script, specifically on the first beam splitter
(OMC:math:`1`\ \_\ :math:`1`) that compose the OMC cavity. The mode
match in the rest of the cavity is provided by the command ‘cav‘ that
defines the cavity. To fix this problem it’s been made a simulation to
minimise the mode mismatch between OMC\ :math:`1`\ \_\ :math:`1` and the
two lenses that serve to mode match it, MMT_L\ :math:`1` and
MMT_L\ :math:`2`. A ‘mmd‘ detector is been used to simultaneously scan
on the two focal lengths. We reached a mode mismatch :math:`\sim 10^6`,
this is enough to confirm that the OMC is in mode-match state, since at
this point the variation on the focal lengths of the lenses is so
negletable that is not manually possible anymore.

Coating and substrate diameters for Ad.Virgo+, Phase I
======================================================

We report here the tables of the designed values with the coating and
substrate diameters used in Ad.Virgo+, Phase I (Ref |VIR-0128A-12 link|_).
To stay updated with the most recent changes, you can see the `Virgo
wiki <https://wiki.virgo-gw.eu/Commissioning/OptChar/Param_sim>`__.

.. |VIR-0128A-12 link| replace:: :math:`VIR-0128A-12`
.. _VIR-0128A-12 link: https://tds.virgo-gw.eu/?content=3&r=9317

.. _fig_coat:

.. figure:: images/coating.png
   :alt:
      A summary of the coatings main features of the Ad.Virgo+ (Phase
      I) mirrors at 1064 nm.´
   :name: fig:coat
   :width: 12cm

   A summary of the coatings main features of the Ad.Virgo+ (Phase I)
   mirrors at 1064 nm.

.. _fig_coat2:

.. figure:: images/mirrors.png
   :alt:
      Specifications of the bulk substrates for the Ad.Virgo+ (Phase
        I) mirrors.
   :name: fig:coat
   :width: 12cm

   Specifications of the bulk substrates for the Ad.Virgo+ (Phase I)
   mirrors.

Using the KatScript
===================

Pretuning the model
-------------------

We bring the model to a sensible configuration by "pretuning", which
used the power of the carrier (main laser beam without RF modulation) to
set the desired resonance conditions.

This is done using following steps:

#. Switch off the modulators to only use main carrier field in the
   following steps.

#. Remove the SR and PR mirrors. This is to remove any cross-coupling
   between the two arms, so we can tune them independently from each
   other.

#. Move the arm cavity end mirrors to maximise power in each arm so the
   arm cavities are on resonance.

#. Move the arm cavities to minimise power at the output so the
   interferometer is at the dark fringe

#. Restore the PR mirror, and move it to maximise power in the PRC so
   the PRC is on resonance.

#. Restore the 56MHz modulator.

#. Restore the SR mirror, and move it to maximise power in B4_112MHz.

#. Restore the other modulators.

#TODO: add a comparison plot between the old and new pre-tuning
strategies.

Update 21/05/2022 - Riccardo
----------------------------

Updated the common kat to add the followings: 1) The RoC of the PRM and
SRM are set to the measured values. 2) The CPs’ focal length has been
tuned accordingly 3) The recycling cavities are defined as “long
cavities”

The CP’s focal length comes from
`VIR-0641A-22 <https://tds.virgo-gw.eu/?content=3&r=20721>`__

Behavior of beam parameters in the PRC
--------------------------------------

This section illustrates the behavior of the beam parameter in the PRC
when defining the cavity as "short" or "long" by including or excluding
the arms.

The following plots were generated using a simplified model of the three
mirror coupled cavity including the PRC and one of the arm cavities.

.. _fig_prc-q-overview:

.. figure:: images/prc-beam-parameters-overview.pdf
   :alt:
      An overview of the behavior of the beam parameter seen through
      the power in the PRC.
   :name: fig:prc-q-overview
   :width: 12cm

   An overview of the behavior of the beam parameter seen through the
   power in the PRC.

.. _fig_prc-q-convergence:

.. figure:: images/prc-beam-parameters-convergence.pdf
   :alt:
      The short cavity with fixed q converges to the long cavity with
      increased maxtem.
   :name: fig:prc-q-convergence
   :width: 12cm

   The short cavity with fixed q converges to the long cavity with
   increased maxtem.

.. _fig_prc-itm-mismatch:

.. figure:: images/prc-beam-parameters-mismatch.pdf
   :alt:
      The short cavity has a mismatch at the ITM whereas the long
      cavity does not.
   :name: fig:prc-itm-mismatch
   :width: 12cm

   The short cavity has a mismatch at the ITM whereas the long cavity
   does not.

.. _fig_prc-laser-w0:

.. figure:: images/prc-beam-parameters-w0.pdf
   :alt:
      The waist size at the laser varies greatly when defining the
      short cavity.
   :name: fig:prc-laser-w0
   :width: 12cm

   The waist size at the laser varies greatly when defining the short
   cavity.

.. _fig_prc-laser-z:

.. figure:: images/prc-beam-parameters-z.pdf
   :alt:
      The waist position at the laser varies greatly when defining
      the short cavity.
   :name: fig:prc-laser-z
   :width: 12cm

   The waist position at the laser varies greatly when defining the
   short cavity.

.. [1]
   Here the Loss indicate the round trip loss in the cavity

.. [2]
   Here the Loss indicate the round trip loss in cavity
