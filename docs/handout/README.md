The handout accompanies the AdV+ common file.

This document serves several functions:

1. Document the current parameters of the interferometer
2. Provide a history of changes and their motivations
3. Document less intuitive behaviors

The code to produce each plot is located in the `code` directory.

The document is written in $\LaTeX$.
